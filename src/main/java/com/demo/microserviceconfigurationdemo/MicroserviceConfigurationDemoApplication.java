package com.demo.microserviceconfigurationdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceConfigurationDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceConfigurationDemoApplication.class, args);
	}

}
